const BlogPost = require('../model/blogs');

exports.getBlogs = (req, res, next) => {
    BlogPost.find()
    .then(result => {
        res.status(200).json({message: 'Data Kebaca', data: result});
        next();
    })
    .catch(err => {
        res.status(404).json({message: 'Invalid Read', data: null});
        next();
    });
}

exports.createBlogs = (req, res, next) => {
    const {title, images, content, users} = req.body;
    if(!title || !images || !content || !users)
        return false;
    const blog = new BlogPost({title, images, content, users});
    blog.save()
    .then(result => {
        res.status(201).json({message: 'POST success', data: result});
        next();
    })
    .catch(err => {
        res.status(401).json({message: "Invalid Input", data: null});
        next();
    });
}