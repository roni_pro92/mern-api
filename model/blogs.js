const monggose = require('mongoose');
const Schema = monggose.Schema;

const BlogPost = new Schema({
    title: {
        type: String,
        required: true,
    },
    images: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    users: {
        type: Object,
        required: true,
    }
}, {
    timestamps: true,
});

module.exports = monggose.model('blogs', BlogPost);