const express = require('express');
const router = express.Router();
const blogController = require('../controller/blogs');

router.get("/", blogController.getBlogs);

router.post("/create", blogController.createBlogs);

module.exports = router;
